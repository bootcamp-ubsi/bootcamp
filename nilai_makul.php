<?php
class NilaiMataKuliah {
    public $namaMataKuliah;
    public $sks;
    public $nilai;

    public $grade = "E";
    public $bobot = 0;
    public $predikat = "Sangat Buruk";
    
    function __construct($nmMakul, $sks, $nilai)
    {
        $this->namaMataKuliah = $nmMakul;
        $this->sks = $sks;
        $this->nilai = $nilai;
    }

    public function prosesHasil()
    {
        if($this->nilai >= 80 && $this->nilai <= 100)
        {
            $this->grade="A";
            $this->bobot=4;
            $this->predikat="Sangat Baik";
        }else if($this->nilai >= 68 && $this->nilai <= 79)
        {
            $this->grade="B";
            $this->bobot=3;
            $this->predikat="Baik";
        }else if($this->nilai >= 56 && $this->nilai <= 67)
        {
            $this->grade="C";
            $this->bobot=2;
            $this->predikat="Cukup";
        }else if($this->nilai >= 31 && $this->nilai <= 55)
        {
            $this->grade="D";
            $this->bobot=1;
            $this->predikat="Buruk";
        }else {
            $this->grade="E";
            $this->bobot=0;
            $this->predikat="Sangat Buruk";
        }
    }

    public function bobotSks()
    {
        return $this->sks*$this->bobot;
    }
}