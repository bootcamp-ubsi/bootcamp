<?php require_once('persegi.php') ?>

<?php
class Balok extends Persegi{
    public $tinggi;

    public function luas()
    {
        $pl = $this->panjang * $this->lebar;
        $pt = $this->panjang * $this->tinggi;
        $lt = $this->lebar * $this->tinggi;
        $hasil = 2 * ( $pl + $pt + $lt );
        echo "Luas Balok : ".$hasil."</br>";
    }
}