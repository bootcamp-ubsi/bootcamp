<?php require_once('nilai_makul.php');?>

<?php
$makul[1] = new NilaiMataKuliah("Dasar Pemrograman",4,85); 
$makul[2] = new NilaiMataKuliah("Web Programming",4,80); 
$makul[3] = new NilaiMataKuliah("Pancasila",3,95); 
$makul[4] = new NilaiMataKuliah("PTIK",3,75); 
?>

NIM : 121220851<br>
Nama : Andi Dharmawan<br>
<table border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th>Mata Kuliah</th>
        <th>SKS</th>
        <th>Nilai</th>
        <th>Grade</th>
        <th>SKS x Bobot</th>
        <th>Predikat</th>
    </tr>
    <?php for($i=1; $i<=4; $i++): ?>
        <?php $makul[$i]->prosesHasil(); ?>   
        <tr>
            <td><?= $makul[$i]->namaMataKuliah; ?></td>
            <td><?= $makul[$i]->sks; ?></td>
            <td><?= $makul[$i]->nilai; ?></td>
            <td><?= $makul[$i]->grade ?></td>
            <td><?= $makul[$i]->bobotSks(); ?></td>
            <td><?= $makul[$i]->predikat; ?></td>
        </tr>
    <?php endfor; ?>
</table>