<?php require_once('nilai_makul.php');?>

NIM : 121220851<br>
Nama : Andi Dharmawan<br>
<table border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th>Mata Kuliah</th>
        <th>SKS</th>
        <th>Nilai</th>
        <th>Grade</th>
        <th>SKS x Bobot</th>
        <th>Predikat</th>
    </tr>
    <?php 
        $dsrPemrograman = new NilaiMataKuliah("Dasar Pemrograman",4,85); 
        $dsrPemrograman->prosesHasil();
    ?>
    <tr>
        <td><?= $dsrPemrograman->namaMataKuliah; ?></td>
        <td><?= $dsrPemrograman->sks; ?></td>
        <td><?= $dsrPemrograman->nilai; ?></td>
        <td><?= $dsrPemrograman->grade ?></td>
        <td><?= $dsrPemrograman->bobotSks(); ?></td>
        <td><?= $dsrPemrograman->predikat; ?></td>
    </tr>
    <?php 
        $web = new NilaiMataKuliah("Web Programming",4,80); 
        $web->prosesHasil();
    ?>
    <tr>
        <td><?= $web->namaMataKuliah; ?></td>
        <td><?= $web->sks; ?></td>
        <td><?= $web->nilai; ?></td>
        <td><?= $web->grade ?></td>
        <td><?= $web->bobotSks(); ?></td>
        <td><?= $web->predikat; ?></td>
    </tr>
    <?php 
        $pkn = new NilaiMataKuliah("Pancasila",3,95); 
        $pkn->prosesHasil();
    ?>
    <tr>
        <td><?= $pkn->namaMataKuliah; ?></td>
        <td><?= $pkn->sks; ?></td>
        <td><?= $pkn->nilai; ?></td>
        <td><?= $pkn->grade ?></td>
        <td><?= $pkn->bobotSks(); ?></td>
        <td><?= $pkn->predikat; ?></td>
    </tr>
    <?php 
        $ptik = new NilaiMataKuliah("PTIK",3,75); 
        $ptik->prosesHasil();
    ?>
    <tr>
        <td><?= $ptik->namaMataKuliah; ?></td>
        <td><?= $ptik->sks; ?></td>
        <td><?= $ptik->nilai; ?></td>
        <td><?= $ptik->grade ?></td>
        <td><?= $ptik->bobotSks(); ?></td>
        <td><?= $ptik->predikat; ?></td>
    </tr>
</table>
